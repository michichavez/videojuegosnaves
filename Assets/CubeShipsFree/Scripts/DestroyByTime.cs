﻿using UnityEngine;
using System.Collections;

namespace CubeSpaceFree{
    public class DestroyByTime : MonoBehaviour{

        public float lifetime;
        
		void Start(){
            Destroy(gameObject, lifetime);
        }

		void Update(){
        }
    }
}