﻿using UnityEngine;
using System.Collections;


namespace CubeSpaceFree{
    public class ScaleDown : MonoBehaviour{
        public float multiplier = 0.98f;  

        public void Update(){
            if (transform.localScale.x>0.2f){
                transform.localScale *= multiplier;
            }
            else{
                Destroy(gameObject);
            }
        }
    }
}
