﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace CubeSpaceFree{
    public class GameController : MonoBehaviour{
        public GameObject[] hazards;        
        public Vector3 spawnValues;         
        public int itemPerHazardCount = 5;  
        public float spawnWait;             
        public float startWait;             
        public float waitWave;              

        public Text scoreText;
        public Button restartButton;
        public Text gameOverText;
        public bool isGameOver = false;
        public int score;

        void Start(){
            score = 0;
            restartButton.gameObject.SetActive(false);
            gameOverText.gameObject.SetActive(false);
            StartCoroutine(SpawnWaves());
            UpdateScore();
        }

        public void Update(){
            if (Input.GetKeyDown(KeyCode.F1)){
                Time.timeScale = 0;
            }
            else if (Input.GetKeyDown(KeyCode.F2)){
                Time.timeScale = 1;
            }
        }

        public void Restart(){
            Application.LoadLevel(Application.loadedLevelName);
        }

        IEnumerator SpawnWaves(){
            yield return new WaitForSeconds(startWait);
            while (true){
                for (int i = 0; i < hazards.Length; i++){
                    GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                    for (int j = 0; j < itemPerHazardCount; j++){
                        Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                        Quaternion spawnRotation = Quaternion.identity;
                        Instantiate(hazard, spawnPosition, spawnRotation);
                        yield return new WaitForSeconds(spawnWait);
                    }
                }

                yield return new WaitForSeconds(waitWave);
                if (isGameOver){
                    break;
                }
            }
        }

        public void AddScore(int newScoreValue){
            score += newScoreValue;
            UpdateScore();
        }

        void UpdateScore(){
            scoreText.text = "Puntaje: " + score;
        }

        public void GameOver(){
            restartButton.gameObject.SetActive(true);
            gameOverText.gameObject.SetActive(true);
        }
    }
}
