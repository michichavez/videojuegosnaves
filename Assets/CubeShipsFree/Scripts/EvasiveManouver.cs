﻿using UnityEngine;
using System.Collections;

namespace CubeSpaceFree{
    public class EvasiveManouver : MonoBehaviour{
        public Vector2 startWait;       
        public Vector2 manouverTime;    
        public Vector2 manouverWait;    
        public float smoothing;         
        private Rigidbody myRigidbody;  
        private float targetManouver;   
        public Boundary boundary;       
        public float dodge;             
        public float tilt;              

        void Start(){
            myRigidbody = GetComponent<Rigidbody>();
            StartCoroutine(Evade());
        }

        IEnumerator Evade(){
            yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));
            while (true){
                targetManouver = Random.Range(1, dodge*-Mathf.Sign(transform.position.x));
                yield return new WaitForSeconds(Random.Range(manouverTime.x, manouverTime.y));
                targetManouver = 0;
                yield return new WaitForSeconds(Random.Range(manouverWait.x, manouverWait.y));
            }
        }

        void FixedUpdate(){
            float newManouver = Mathf.MoveTowards(myRigidbody.velocity.x, targetManouver, Time.deltaTime * smoothing);
            myRigidbody.velocity = new Vector3(newManouver, 0, myRigidbody.velocity.z);
            myRigidbody.position = new Vector3(
                    Mathf.Clamp(myRigidbody.position.x, boundary.xMin, boundary.xMax),
                    0,
                    Mathf.Clamp(myRigidbody.position.z, boundary.zMin-20, boundary.zMax+20)  
                );
            myRigidbody.rotation = Quaternion.Euler(0, 0, myRigidbody.velocity.x * -tilt);
        }
    }
}
